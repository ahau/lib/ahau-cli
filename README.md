# ahau-cli

## Usage

Install Node 14 LTS with e.g. [nvm](https://github.com/nvm-sh/nvm) or [nvm-windows](https://github.com/coreybutler/nvm-windows)


You will then see logged out:
- `host` : the hostname your pataka will be connected to over (you generally want this to be accessible on the internet)
- `port` : the local port your pataka will be listening on
- `feedId` : the unique scuttlebutt id of this pataka (for debugging)
- `data` : path to where data is being stored for this instance 
- `config` : path to where you can persist custom config

To use this in the cloud, you might like to launch it with a tool like: [pm2](https://www.npmjs.com/package/pm2)

## Config

You can modify which `port` and `hostname` are used if by editing the `config` file output above.
Here's en example config (note all fields are optional)

```json
{
  "port": "8068",
  "pataka": {
    "host": "mydomain.nz"
  }
}
```

You can also use ENV VAR (these will over-ride the config file):
- `PORT` (default: 8088)
- `AHAU_HOST`
- `AHAU_WEB_PORT` (default: 3000)
- `AHAU_LOG` controls logging, set to `true` to enable

NOTE: it's currently not only possible to set the local port the patkaka listens on - you cannot set the external port the invite code will use
If this external port is different you can manually edit the port invite code.

## :warning: WARNING

This module currently exposes an admin web page **without authentication**.

## TODO

0. Add Authentication
1. update ssb-ahau so i dont need to edit node_modules
2. find a way to not commit the raw `public/` folder

### Development

to start in development ahau env:
```bash
$ npm run dev
```

to run in production ahau env:
update /node_modules/ahau-graphql-server/index  

```
line 19: const cliOrigin = `http://localhost:3001`
...
line 37: if (origin === cliOrigin) return cb(null, true) // GraphiQL
```
 

```bash
$ git clone https://gitlab.com/ahau/lib/ahau-cli.git
$ cd ahau-cli
$ npm i
$ npm start
```

To update the dependencies to be in line with `pataka`
```bash
$ npm link pataka
$ npm run syncDeps
```
