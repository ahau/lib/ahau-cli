const path = require('path')
const env = require('ahau-env')()
const chalk = require('chalk')
const boxen = require('boxen')
const Config = require('./ssb.config')
const karakia = require('./karakia')

module.exports = function startAhau () {
  /* Karakia tūwhera */
  karakia()

  const stack = require('secret-stack')(env.ahau.caps)
  .use(require('ssb-db'))

  .use(require('ssb-conn'))
  .use(require('ssb-lan'))
  .use(require('ssb-replicate'))
  .use(require('ssb-friends'))

  .use(require('ssb-blobs'))
  .use(require('ssb-serve-blobs'))
  .use(require('ssb-hyper-blobs'))

  .use(require('ssb-query'))
  .use(require('ssb-backlinks'))

  .use(require('ssb-tribes'))
  .use(require('ssb-tribes-registration'))

  .use(require('ssb-profile'))
  .use(require('ssb-settings'))
  .use(require('ssb-story'))
  .use(require('ssb-artefact'))
  .use(require('ssb-whakapapa'))

  .use(require('ssb-invite'))
  .use(require('ssb-ahau'))
  .use(require('ssb-recps-guard'))

  const config = Config()
  const server = stack(config)

  printConfig(server)
  return server
}

// function printConfig (server) {
//   const { config, id } = server
//   const envName = env.isProduction ? '' : ` ${env.name.toUpperCase()} `

//   const configTxt = chalk`{blue AHAU} {white.bgRed ${envName}}

// {bold host}    ${config.host}
// {bold port}    ${config.port}
// {bold feedId}  ${id}
// {bold data}    ${config.path}
// {bold config}  ${path.join(config.path, 'config')}`

//   console.log(boxen(configTxt, {
//     padding: 1,
//     margin: 1,
//     borderStyle: 'round',
//     borderColor: 'blue'
//   }))
// }

function printConfig (server) {
  const { config, id } = server

  const envName = env.isProduction ? '' : ` ${env.name.toUpperCase()} `

  const configTxt = chalk`{blue AHAU} {white.bgRed ${envName}}

{bold feedId}  ${id}
{bold path}    ${config.path}
{bold network}
  ├── host  ${config.host}
  ├── port  ${config.port}
  └── api   http://localhost:${config.graphql.port}/graphql
`

  console.log(boxen(configTxt, {
    padding: 1,
    margin: 1,
    borderStyle: 'round',
    borderColor: 'blue'
  }))
}
