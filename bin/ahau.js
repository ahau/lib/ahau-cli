#!/usr/bin/env node

const chalk = require('chalk')
const startSSB = require('..')
const startExpress = require('../express')

const { AHAU_LOG, AHAU_WEB_PORT } = process.env

const ssb = startSSB()
startExpress(AHAU_WEB_PORT || 3001)

if (AHAU_LOG) {
  console.log(chalk`{blue logging started...}`)
  ssb.post(m => console.log(m.value.author, m.value.sequence))
}
